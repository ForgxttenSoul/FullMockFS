const normalize = require("./normalize.js");
const _ = require("lodash");
const deepclone = require("clone");
const istream = require("stream");
class fs {
    constructor(op = {}) {
        this._.fs = this._template_filesystem();
        this._.fd = [];
        this._.usePerms = op.usePerms || false;
        this._perms("init");
        this._users("init");
        this._groups("init");
    }
}
fs.prototype.usePerms = (function (inp = 0) {
    if (inp !== 0) {
        this._.usePerms = inp;
    } else {
        return (this._.usePerms);
    }
});
module.exports = fs;
fs.prototype._ = {};
fs.prototype.constants = {
    O_RDONLY: 0,
    O_WRONLY: 1,
    O_RDWR: 2,
    S_IFMT: 61440,
    S_IFREG: 32768,
    S_IFDIR: 16384,
    S_IFCHR: 8192,
    S_IFLNK: 40960,
    O_CREAT: 256,
    O_EXCL: 1024,
    O_TRUNC: 512,
    O_APPEND: 8,
    F_OK: 0,
    R_OK: 4,
    W_OK: 2,
    X_OK: 1
};
fs.prototype._true = (() => { return (true); });
fs.prototype._false = (() => { return (false); });
fs.prototype._clone = deepclone;
fs.prototype._to_base64 = (function (inp) { return (Buffer.from(inp).toString("base64")); });
fs.prototype._from_base64 = (function (inp) { return (Buffer.from(inp, "base64")); });
fs.prototype._reservedChars = `\\/:?<>"|`.split("");
fs.prototype._clone2 = (function (inp) { return (JSON.parse(JSON.stringify(inp))); });
fs.prototype.permid = (function (permid = 0) {
    if (permid === 0) {
        return (this._.cpermid);
    }
    if (this._perms("fromPermID", permid) !== false) { this._.cpermid = permid; return (true); }
    return (false);
});
fs.prototype._perms = (function (...args) {
    if (args[0] === "init") {
        this._.permids = {};
        this._.permids[`${new Date().getTime()}`] = "0:0";
    }
    if (args[0] === "toPermID") {
        const permid = false;
        Object.keys(this._.permids).forEach((_permid, _ind) => this._.permids[_permid] === args[1] ? permid = _permid : false);
        return (permid);
    }
    if (args[0] === "fromPermID") {
        return (this._.permids[args[1]] || false);
    }
    if (args[0] === "list") {
        const permids = {};
        Object.keys(this._.permids).forEach(obj => {
            const permid = this._.permids[obj];
            permids[obj] = {
                user: this._users("fromUid", parseInt(permid.split(":")[0])),
                group: this._groups("fromGid", parseInt(permid.split(":")[1])),
                uid: parseInt(permid.split(":")[0]),
                gid: parseInt(permid.split(":")[1])
            };
        });
        return (permids);
    }
});
fs.prototype._users = (function (...args) {
    if (args[0] === "init") {
        this._.uids = { 0: [0, "root", 0] };
    }
    if (args[0] === "toUid") {
        let uid = false;
        let def = 0;
        if (args[2] !== undefined) { if (args[2] === "gid") { def = 2; } }
        Object.keys(this._.uids).forEach((_uid, _ind) => this._.uids[_uid][1] === args[1] ? uid = this._.uids[_uid][def] : false);
        if (typeof (uid) === "number") {
            uid = this._groups("toGid", args[1]);
        }
        return (uid);
    }
    if (args[0] === "fromUid") {
        return (this._.uids[args[1]][1] || false);
    }
});
fs.prototype._groups = (function (...args) {
    if (args[0] === "init") {
        this._.gids = { 0: "root" };
    }
    if (args[0] === "toGid") {
        let gid = false;
        Object.keys(this._.gids).forEach((_gid, _ind) => this._.gids[_gid] === args[1] ? gid = _gid : false);
        return (gid);
    }
    if (args[0] === "fromGid") {
        return (this._.gids[args[1]] || false);
    }
});
/*#errors*/
fs.prototype.errcode = ((num = 0) => {
    switch (num) {
        case -1: return ("Path doesn't exist.");
        case -2: return ("Path not directory.");
        case -3: return ("Path not file.");
        case -4: return ("Invalid path.");
        case -5: return ("Invalid user.");
        case -6: return ("Read access denied.");
        case -7: return ("Directory already exists.");
        case -8: return ("Parent directory doesn't exist.");
        case -9: return ("Parent not directory.");
        case -10: return ("File already exists.");
        case -11: return ("Path already exists.");
        case -12: return ("Flags not defined.");
        case -13: return ("File Descriptor not open to Write.");
        case -14: return ("File Descriptor not open to Read.");
        case -15: return ("File Descriptor doesn't exist.");
        case -16: return ("File Descriptor not open.");
        case -17: return ("File locked.");
        case -18: return ("PAth not symbolic link.");
        default: return ("No/invalid errno supplied.");
    }
});
fs.prototype._pathToArray = (function (path = "/") {
    path = normalize(path);
    const nix = /^\//.test(path);
    if (!nix) {
        if (!/^[A-Za-z]:/.test(path)) {
            return (false);
            throw new Error(`Invalid path: '${path}'`);
        }
        path = path.replace(/[\\\/]+/g, "\\");
        path = path.split(/[\\\/]/);
        path[0] = path[0].toUpperCase().replace(":", "");
    } else {
        path = path.replace(/\/+/g, "/");
        path = path.substr(1).split("/");
    }
    if (!path[path.length - 1]) { path.pop(); }
    return (path);
});
fs.prototype._arrayToLodash = (function (path = []) {
    if (path === false) { return (-4); }
    path.forEach((__, i) => path[i] = __.replace(/\./gim, "/"));
    let _path = `_.fs.contents.${path.join(".contents.")}`;
    if (path.length === 0) { _path = "_.fs"; }
    return (_path);
});
fs.prototype._permissions = (function (...args) {
    const type = args[0];
    const p = {
        r: { type: "read", other: 4 },
        4: { type: "read", other: "r" },
        w: { type: "write", other: 2 },
        2: { type: "write", otther: "w" },
        x: { type: "execute", other: 1 },
        1: { type: "execute", other: "x" },
        l: { type: "symboliclink" },
        "-": { type: "n/a" }
    };
    if (type === "expand") {
        const perms = arg[1].split("");
        const plist = {
            type: p[perms[0]].type,
            user: {
                read: p[perms[1]].type === "read",
                write: p[perms[2]].type === "write",
                execute: p[perms[3]].type === "execute"
            },
            group: {
                read: p[perms[4]].type === "read",
                write: p[perms[5]].type === "write",
                execute: p[perms[6]].type === "execute"
            },
            other: {
                read: p[perms[7]].type === "read",
                write: p[perms[8]].type === "write",
                execute: p[perms[9]].type === "execute"
            }
        };
        return (plist);
    }
});
fs.prototype._template_perm = (function (type) {
    /*
        - none
        l - symlinks
        d - directory
        r - read(4)
        w - write(2)
        x - execute(1)
        ugo - user:group:other
    */
    const read = ["r", 4];
    const write = ["w", 2];
    const execute = ["x", 1];
    const p = {
        r: { type: "read", other: 4 },
        4: { type: "read", other: "r" },
        w: { type: "write", other: 2 },
        2: { type: "write", otther: "w" },
        x: { type: "execute", other: 1 },
        1: { type: "execute", other: "x" },
    };
    if (type === "file") { return ("-rw-r--r--"); }
    if (type === "directory") { return ("drwxrw-r--"); }
    if (type === "symboliclink") { return ("lrwxrwxrwx"); }
    return (false);
});
fs.prototype._template_file = (function () {
    return (this._clone({
        ":": { type: "file", size: 0, lock: false, owner: { group: 0, user: 0 }, perms: this._template_perm("file"), mtime: this._time(), ctime: this._time(), atime: this._time(), birthtime: this._time() },
        contents: this._to_base64("")
    }));
});
fs.prototype._template_directory = (function () {
    return (this._clone({
        ":": { type: "directory", size: 2048, lock: false, owner: { group: 0, user: 0 }, perms: this._template_perm("directory"), mtime: this._time(), ctime: this._time(), atime: this._time(), birthtime: this._time() },
        contents: {}
    }));
});
fs.prototype._template_symboliclink = (function () {
    return (this._clone({
        ":": { type: "symboliclink", size: 2048, lock: false, target: "", owner: { group: 0, user: 0 }, perms: this._template_perm("symboliclink"), mtime: this._time(), ctime: this._time(), atime: this._time(), birthtime: this._time() },
        contents: {}
    }));
});
fs.prototype._template_filesystem = (function () {
    return (this._clone({
        ":": { type: "filesystem", size: 2048, symlinks: {}, mounts: {}, owner: { group: 0, user: 0 }, perms: this._template_perm("directory"), mtime: this._time(), ctime: this._time(), atime: this._time(), birthtime: this._time() },
        contents: {}
    }));
});
fs.prototype._template_stat = (function (path) {
    path = this.resolve(path.toString() || path);
    const meta = this.meta(path)[":"];
    return ({
        dev: 0, ino: 0, mode: 0, nlink: 1, rdev: 0, blocks: 1,
        uid: meta.owner.user,
        gid: meta.owner.group,
        size: meta.size,
        atime: meta.atime,
        ctime: meta.ctime,
        mtime: meta.mtime,
        birthtime: meta.birthtime
    });
});
fs.prototype.meta = (function (path = "", obj = null) {
    const literal = path.substr(0, 1) === ":";
    if (literal) {
        path = path.substr(1, path.length);
    }
    path = this.resolve(path.toString() || path);
    const _path = this._arrayToLodash(this._pathToArray(path));
    if (_path === false) { return (-4); }
    const out = _.get(this, _path, -1);
    if (obj === null) {
        if (out === -1) { return (-1); }
        if (out[":"].type === "symboliclink" && !literal) {
            return (this.meta(out[":"].target));
        }
        return (out);
    } else if (obj === false) {
        _.unset(this, _path);
    } else {
        _.setWith(this, _path, obj);
    }
});
fs.prototype._time = (() => new Date());
fs.prototype._checkAccess = (function (path, uid, gid) {
    path = this.resolve(path.toString() || path);
    const meta = this.meta(path);
    if (meta === false) { return (-1); }
    const meta_user = meta[":"].owner.user;
    const meta_group = meta[":"].owner.group;
    let accessType = "other";
    if (meta_group === gid) { accessType = "group"; }
    if (meta_user === uid) { accessType = "user"; }
    let p, user;
    switch (accessType) {
        case "user":
            p = meta[":"].perms;
            user = p.substr(1, 3).split("");
            return ({ accessType, read: user[0] === "r", write: user[1] === "w", execute: user[2] === "x" });
            break;
        case "group":
            p = meta[":"].perms;
            user = p.substr(1 + 3, 3).split("");
            return ({ accessType, read: user[0] === "r", write: user[1] === "w", execute: user[2] === "x" });
            break;
        case "other":
            p = meta[":"].perms;
            user = p.substr(1 + 6, 3).split("");
            return ({ accessType, read: user[0] === "r", write: user[1] === "w", execute: user[2] === "x" });
            break;
    }
    return (false);
});
fs.prototype._checkPermission = (function (path, type = 0) {
    path = this.resolve(path.toString() || path);
    if (this._.usePerms !== true) { return (true); }
    const permid = this._.cpermid;
    if (type === 0) { throw new Error("Please provide type for _checkPermission"); }
    const p = this._perms("fromPermID", permid);
    if (permid !== 0) {
        if (!p) { return (-5); }
        const uid = parseInt(p.split[0]);
        const gid = parseInt(p.split[1]);
        if (!this._checkAccess(path, uid, gid)[type]) { return (-6); }
    }
    return (true);
});

fs.prototype.statSync = (function (path) {
    path = this.resolve(path.toString() || path);
    const meta = this.meta(path);
    if (meta === -1) { return (-1); }
    const access = this._checkPermission(path, "read");
    if (access !== true) { return (access); }
    if (meta[":"].type === "symboliclink") {
        return (this.statSync(meta[":"].target));
    } // this is for statSync only
    const stat = this._template_stat(path);
    stat.isFile = (() => ["file"].indexOf(meta[":"].type) > -1);
    stat.isDirectory = (() => ["directory", "filesystem"].indexOf(meta[":"].type) > -1);
    stat.isSymbolicLink = (() => ["symboliclink"].indexOf(meta[":"].type) > -1);
    stat.isCharacterDevice = this._false;
    stat.isBlockDevice = this._false;
    stat.isFIFO = this._false;
    stat.isSocket = this._false;
    return (stat);
});
fs.prototype.lstatSync = (function (path) {
    path = this.resolve(path.toString() || path);
    const meta = this.meta(path);
    if (meta === -1) { return (-1); }
    const access = this._checkPermission(path, "read");
    if (access !== true) { return (access); }
    const stat = this._template_stat(path);
    stat.isFile = (() => ["file"].indexOf(meta[":"].type) > -1);
    stat.isDirectory = (() => ["directory", "filesystem"].indexOf(meta[":"].type) > -1);
    stat.isSymbolicLink = (() => ["symboliclink"].indexOf(meta[":"].type) > -1);
    stat.isCharacterDevice = this._false;
    stat.isBlockDevice = this._false;
    stat.isFIFO = this._false;
    stat.isSocket = this._false;
    return (stat);
});
fs.prototype.existsSync = (function (path) {
    path = this.resolve(path.toString() || path);
    const meta = this.meta(path);
    if (meta === -1) { return (-1); }
    const access = this._checkPermission(path, "read");
    if (access !== true) { return (access); }
    return (true);
});
fs.prototype.readdirSync = (function (path, encoding = "utf8") {
    path = this.resolve(path.toString() || path);
    const meta = this.meta(path);
    if (meta === -1) { return (-1); }
    const access = this._checkPermission(path, "read");
    if (access !== true) { return (access); }
    if (meta[":"].type === "symboliclink") {
        return (this.readdirSync(meta[":"].target, encoding));
    } else {
        if (!this.statSync(path).isDirectory()) { return (-2); }
        const files = Object.keys(meta.contents);
        files.forEach((file, i) => files[i] = file.replace(/\//gim, "."));
        switch (encoding.encoding || encoding) {
            case "buffer": files.forEach((file, i) => files[i] = Buffer.from(file)); break;
            default: break;
        }
        return (files);
    }
});
fs.prototype.accessSync = (function (path) {
    path = this.resolve(path.toString() || path);
    const meta = this.meta(path);
    if (meta === -1) { return (-1); }
    const access = this._checkPermission(path, "read");
    if (access !== true) { return (access); }
    return (true);
});
fs.prototype._fromModeInt = (function (mode) {
    let p = "";
    const imode = `${mode}`.split("");
    let user = imode.length == 3
        ? imode[0]
        : 0;
    let group = imode.length == 3
        ? imode[1]
        : imode.length == 2
            ? imode[0]
            : 0;
    let other = imode.length == 3
        ? imode[2]
        : imode.length == 2
            ? imode[1]
            : imode.length == 1
                ? imode[0]
                : 0;
    if (user >= 4) { user -= 4; p += "r"; } else { p += "-"; }
    if (user >= 2) { user -= 2; p += "w"; } else { p += "-"; }
    if (user >= 1) { user -= 1; p += "x"; } else { p += "-"; }
    if (group >= 4) { group -= 4; p += "r"; } else { p += "-"; }
    if (group >= 2) { group -= 2; p += "w"; } else { p += "-"; }
    if (group >= 1) { group -= 1; p += "x"; } else { p += "-"; }
    if (other >= 4) { other -= 4; p += "r"; } else { p += "-"; }
    if (other >= 2) { other -= 2; p += "w"; } else { p += "-"; }
    if (other >= 1) { other -= 1; p += "x"; } else { p += "-"; }
    return (p);
});
fs.prototype.chmodSync = (function (path, mode) {
    path = this.resolve(path.toString() || path);
    const meta = this.meta(path);
    if (meta === -1) { return (-1); }
    const access = this._checkPermission(path, "write");
    if (access !== true) { return (access); }
    const p = meta[":"].perms.split("")[0] + this._fromModeInt(mode);
    console.log(p);
    meta[":"].perms = p;
    this.meta(path, meta);
    return (true);
});
fs.prototype.chownSync = (function (path, uid = -1, gid = -1) {
    path = this.resolve(path.toString() || path);
    const meta = this.meta(path);
    if (meta === -1) { return (-1); }
    const access = this._checkPermission(path, "write");
    if (access !== true) { return (access); }
    if (this._.uids[uid] !== undefined && this._.gids[gid] !== undefined) {
        meta[":"].owner.user = uid;
        meta[":"].owner.group = gid;
        this.meta(path, meta);
    }
});
fs.prototype.mkdirSync = (function (path, mode = 755) {
    path = this.resolve(path.toString() || path);
    const meta = this.meta(path);
    if (meta !== -1) { return (-7); }
    const access = this._checkPermission(path, "write");
    if (access !== true) { return (access); }
    let parent = path.split("/");
    parent.pop();
    parent = this.statSync(parent.join("/") || "/");
    if (parent === -1) { return (-8); }
    if (!parent.isDirectory()) { return (-9); }
    const tempdir = this._template_directory();
    tempdir[":"].perms = "d" + this._fromModeInt(mode);
    this.meta(path, tempdir);
    return (true);
});
fs.prototype.rmdirSync = (function (path) {
    path = this.resolve(path.toString() || path);
    const meta = this.meta(path);
    if (meta === -1) { return (-1); }
    const access = this._checkPermission(path, "write");
    if (access !== true) { return (access); }
    if (!this.statSync(path).isDirectory()) { return (-2); }
    this.meta(path, false);
    return (true);
});
fs.prototype.copyFileSync = (function (src, dest, flags = 0) {
    src = this.resolve(src.toString() || src);
    dest = this.resolve(dest.toString() || dest);
    const meta = this.meta(src);
    if (meta === -1) { return (-1); }
    const access = this._checkPermission(src, "read");
    if (access !== true) { return (access); }
    if (!this.statSync(src).isFile()) { return (-3); }
    let parent = dest.split("/");
    parent.pop();
    parent = this.statSync(parent.join("/") || "/");
    if (parent === -1) { return (-8); }
    if (!parent.isDirectory()) { return (-9); }
    access = this._checkPermission(parent, "write");
    if (access !== true) { return (access); }
    if (flags !== 0) {
        if (this.existsSync(dest)) {
            return (-10);
        }
    }
    if (this.existsSync(dest)) {
        if (!this.statSync(dest).isfile()) {
            return (-3);
        }
        if (access = this._checkPermission(dest, "write") !== true) {
            return (access);
        }
    }
    const fromMeta = this.meta(src);
    this.meta(dest, fromMeta);
    return (true);
});
fs.prototype.resolve = (function (path) {
    path = this._pathToArray(path);
    path = path.join("/");
    return (path.substr(0, 1) === "/" ? path : "/" + path);
});
fs.prototype.symlinkSync = (function (from, to) {
    from = this.resolve(from.toString() || from);
    to = this.resolve(to.toString() || to);
    if (this.meta(from) === -1) { return (-1); }
    if (access = this._checkPermission(from, "read") !== true) { return (access); }
    if (this.existsSync(to) !== -1) { return (-11); }
    let parent = to.split("/");
    parent.pop();
    parent = parent.join("/");
    if (!this.existsSync(this.resolve(parent))) { return (-8); }
    if (access = this._checkPermission(parent, "write") !== true) { return (access); }
    const newSym = this._template_symboliclink();
    newSym[":"].target = from;
    newSym.contents = this.meta(from)[":"].type === "file" ? this.meta(from).contents : this.meta(from);
    this.meta(to, newSym);
    return (true);
});
fs.prototype.openSync = (function (path, flags, mode = 644) {
    path = this.resolve(path.toString() || path);
    //if(this.meta(path)[":"].lock){return(-17);}
    const accesst = [];
    const base = flags.substr(0, 1) || "";
    switch (base) {
        case "r":
            accesst.push("read");
            break;
        case "w":
            accesst.push("write");
            break;
        case "a":
            accesst.push("append");
            accesst.push("write");
            break;
        default:
            return (-12);
    }
    const sub = flags.substr(1, 1) || "";
    switch (sub) {
        case "x":
            accesst.push("exist");
            break;
        case "+":
            if (accesst.indexOf("read") > -1) { accesst.push("write"); }
            if (accesst.indexOf("write") > -1) { accesst.push("read"); }
            break;
        case "s":
            // cannot provide support for this
            break;
        default:
            break;
    }
    const ext = flags.substr(2, 1) || "";
    switch (ext) {
        case "+":
            if (sub === "+") { break; }
            if (accesst.indexOf("read") > -1) { accesst.push("write"); }
            if (accesst.indexOf("write") > -1) { accesst.push("read"); }
        default:
            break;
    }
    if (meta = this.meta(this.resolve(path + "/..")) !== -1) {
        if (!this.existsSync(path)) {
            if (accesst.indexOf("read") > -1) {
                if (access = this._checkPermission(this.resolve(path + "/.."), "read") !== true) { return (access); }
            }
            if (accesst.indexOf("write") > -1) {
                if (access = this._checkPermission(this.resolve(path + "/.."), "write") !== true) { return (access); }
            }
        }
    } else {
        return (-8);
    }
    // ========================= //
    if (accesst.indexOf("exist") > -1) {
        if (this.existsSync(path)) { return (-10); }
    }
    if (!this.existsSync(path)) {
        this._touch(path, mode);
    }
    const meta = this.meta(path);
    if (meta === -1) { return (-0); }
    if (accesst.indexOf("read") > -1) {
        if (access = this._checkPermission(path, "read") !== true) { return (access); }
    }
    if (accesst.indexOf("write") > -1) {
        if (access = this._checkPermission(path, "write") !== true) { return (access); }
    }
    const fd = {
        path,
        open: true,
        read: accesst.indexOf("read") > -1,
        write: accesst.indexOf("write") > -1,
        offset: accesst.indexOf("append") > -1 ? Buffer.from(meta.content, "base64").length : 0
    };
    const index = this._.fd.length;
    this._.fd[index] = fd;
    this.meta(fd.path)[":"].lock = true;
    return (index);
});
fs.prototype._touch = (function (path, mode = 644) {
    if (meta = this.meta(this.resolve(path + "/..")) === -1) { return (-1); }
    if (access = this._checkPermission(this.resolve(path + "/.."), "write") !== true) { return (access); }
    const tfile = this._template_file();
    tfile[":"].perms = "-" + this._fromModeInt(mode);
    this.meta(path, tfile);
    return (true);
});
fs.prototype.closeSync = (function (fd) {
    fd_data = this._.fd[fd] || false;
    if (!fd_data) { return (-15); }
    //this.meta(fd_data.path)[":"].lock=false;
    fd_data.open = false;
    fd_data.path = "";
    this._.fd[fd].open = fd_data;
    return (true);
});
const insertBuffer1 = (function (f1, f2, insertAt = 0) {
    f1 = f1.toJSON();
    f2 = f2.toJSON();
    const total = [];
    const p1 = f2.data.slice(0, insertAt);
    const p2 = f2.data.slice(insertAt, f2.data.length);
    return (Buffer.from(total.concat(p1).concat(f1.data).concat(p2)));
});
fs.prototype.writeSync = (function (fd, buffer, offset, length, position, encoding = "utf8") {
    fd = this._.fd[fd] || false;
    if (!fd) { return (-15); }
    if (!fd.open) { return (-16); }
    if (!fd.write) { return (-13); }
    buffer = Buffer.from(buffer);
    if (position === undefined) {
        position = offset || fd.offset;
        encoding = length;
        offset = 0;
        length = buffer.length;
    } else {
        offset = offset || fd.offset;
    }
    let meta = this.meta(fd.path);
    if (meta === -1) { this.closeSync(fd); return (-1); }
    const target = fd.path;
    meta = this.meta(target);
    const fromBuffer = Buffer.from(buffer.toJSON().data.splice(offset, length - offset));
    const toBuffer = insertBuffer1(fromBuffer, Buffer.from(meta.contents, "base64"), position);
    meta.contents = Buffer.from(toBuffer).toString("base64");
    fd.offset = Buffer.from(buffer).length;
    return (true);
});
fs.prototype.writeFileSync = (function (path, data, options = {}) {
    path = this.resolve(path.toString() || path);
    options = {
        mode: options.mode || 644,
        flags: options.flags || "w",
        encoding: options.encoding || options || "utf8"
    };
    let meta = this.meta(path);
    let access;
    if (meta === -1) {
        access = this._checkPermission(path + "/..", "write");
        if (access !== true) { return (access); }
        this._touch(path, options.mode);
        meta = this.meta(path);
        if (meta === -1) { return (-1); }
    }
    access = this._checkPermission(path, "write");
    if (access !== true) { return (access); }
    if (this.statSync(path).isDirectory()) { return (-3); }
    meta.contents = Buffer.from(data, options.encoding).toString("base64");
    return (true);
});
fs.prototype.readFileSync = (function (path, options) {
    options = {
        flags: options !== undefined ? options.flags : "r",
        encoding: options !== undefined ? options.encoding || options : "buffer"
    };
    const meta = this.meta(path);
    if (meta === -1) { return (-1); }
    const access = this._checkPermission(path, "read");
    if (access !== true) { return (access); }
    if (this.statSync(path).isDirectory()) { return (-3); }
    const data = meta.contents;
    let buffer = Buffer.from(data, "base64");
    buffer = options.encoding === "buffer" ? buffer : buffer.toString(options.encoding);
    return (buffer);
});
fs.prototype.linkSync = (function (_old, _new) {
    _old = this.resolve(_old.toString() || _old);
    _new = this.resolve(_new.toString() || _new);
    const oldmeta = this.meta(`:${_old}`);
    const newmeta = this.meta(_new);
    if (newmeta === -1) { return (-1); }
    return (this.symlinkSync(_old, _new));
});
fs.prototype.unlinkSync = (function (path) {
    path = this.resolve(path.toString() || path);
    const meta = this.meta(`${path}`);
    if (meta === -1) { return (-1); }
    if (meta[":"].type === "file") {
        this.meta(path, false);
        this.meta(`:${path}`, false);
        return (true);
    } else {
        return (-3);
    }
});
fs.prototype.appendFileSync = (function (path, content, options) {
    path = this.resolve(path.toString() || path);
    options = {
        mode: options.mode || 644,
        flags: options.flags || "a",
        encoding: options.encoding || options || "utf8"
    };
    // [11:47 AM] Z3R05EC_R30X: laurynjuste@yahoo.fr:papamam origin acc with mass effect 2
    const meta = this.meta(path);
    let access;
    if (meta === -1) {
        access = this._checkPermission(path + "/..", "write");
        if (access !== true) { return (access); }
        access = this._checkPermission(path + "/..", "read");
        if (access !== true) { return (access); }
        this._touch(path, options.mode);
        meta = this.meta(path);
        if (meta === -1) { return (-1); }
    }
    access = this._checkPermission(path, "write");
    if (access !== true) { return (access); }
    access = this._checkPermission(path, "read");
    if (access !== true) { return (access); }
    let temp = Buffer.from(data, options.encoding);
    const t1 = Buffer.from(meta.content, "base64");
    temp = this.insertBuffer1(temp, t1, t1.length);
    meta.content = Buffer.from(temp).toString("base64");
    return (true);
});
fs.prototype.readlinkSync = (function (path, options = {}) {
    path = this.resolve(path.toString() || path);
    options = {
        encoding: options.encoding || options || "utf8"
    };
    const meta = this.meta(`:${path}`);
    if (meta === -1) { return (-1); }
    const access = this._checkPermission(path, "read");
    if (access !== true) { return (access); }
    if (meta[":"].type !== "symboliclink") { return (-18); }
    let link = Buffer.from(meta[":"].target);
    return (options.encoding === "buffer" ? link : link.toString(options.encoding));
});
fs.prototype.realpathSync = (function (path, options = {}) {
    path = this.resolve(path.toString() || path);
    options = {
        encoding: options.encoding || options || "utf8"
    };
    let meta = this.meta(`:${path}`);
    if (meta !== -1) {
        path = meta[":"].target;
    } else {
        meta = this.meta(path);
        if (meta === -1) { return (-1); }
    }
    access = this._checkPermission(path, "read");
    if (access !== true) { return (access); }
    let link = Buffer.from(path);
    return (options.encoding === "buffer" ? link : link.toString(options.encoding));
});
fs.prototype.renameSync = (function (oldPath, newPath) {
    oldPath = this.resolve(oldPath.toString() || oldPath);
    newPath = this.resolve(newPath.toString() || newPath);
    const oldMeta = this.meta(oldPath);
    const newMeta = this.meta(newPath);
    if (oldMeta === -1 || newMeta === -1) { return (-1); }
    let access = this._checkPermission(oldPath, "read");
    if (access !== true) { return (access); }
    access = this._checkPermission(newPath, "read");
    if (access !== true) { return (access); }
    access = this._checkPermission(oldPath, "write");
    if (access !== true) { return (access); }
    access = this._checkPermission(newPath, "write");
    if (access !== true) { return (access); }
    this.meta(newPath, oldMeta);
    this.meta(oldPath, false);
    return (true);
});
fs.prototype.truncateSync = (function (path, len = 0) {
    path = this.resolve(path.toString() || path);
    const meta = this.meta(path);
    if (meta === -1) { return (-1); }
    let access = this._checkPermission(path, "read");
    if (access !== true) { return (access); }
    access = this._checkPermission(path, "write");
    if (access !== true) { return (access); }
    if (this.statSync(path).isDirectory()) { return (-3); }
    let data = this.readFileSync(path, "buffer");
    data = data.toJSON().data;
    data = data.splice(0, len);
    data = Buffer.from(data);
    this.writeFileSync(path, data);
    return (true);
});
fs.prototype.utimesSync = (function (path, atime, mtime) {
    path = this.resolve(path.toString() || path);
    const meta = this.meta(path);
    if (meta === -1) { return (-1); }
    let access = this._checkPermission(path, "read");
    if (access !== true) { return (access); }
    access = this._checkPermission(path, "write");
    if (access !== true) { return (access); }
    console.log(meta);
    meta[":"].atime = new Date(atime);
    meta[":"].mtime = new Date(mtime);
    this.meta(path, meta);
    return (true);
});
const insertBuffer2 = (function (f1, f2, insertAt = 0) {
    f1 = f1.toJSON();
    f2 = f2.toJSON();
    const arr = [];
    for (let i = 0; i < insertAt; i++) {
        arr.push(f2.data[i]);
    }
    for (let i = 0; i < f1.data.length; i++) {
        arr.push(f1.data[i]);
    }
    for (let i = insertAt; i < f2.data.length; i++) {
        arr.push(f2.data[i]);
    }
    return (Buffer.from(arr));
});
const coms = "mkdir,rmdir,readdir,writeFile,readFile,stat,access,appendFile,chmod,chown,close,copyFile,link,lstat,open,readlink,realpath,rename,symlink,truncate,unlink,utimes,write,read".split(",");
coms.forEach($ => {
    fs.prototype[$] = (function (...options) {
        setTimeout($$ => {
            console.log(options);
            const cb = options.pop();
            cb(this[`${$}Sync`](...options));
        }, 1);
    });
});
fs.prototype.$export = (function () { return (Buffer.from(JSON.stringify(this._.fs)).toString("base64")); });
fs.prototype.$import = (function ($) { this._.fs = JSON.parse(Buffer.from($, "base64").toString("utf8")); });
fs.prototype.readStream = readStream = ($) => {
    $ = Buffer.from($) || $ || Buffer.from("");
    const rstream = new istream.Readable();
    rstream._read = $$ => $$;
    rstream.push($);
    rstream.push(null);
    return (rstream);
};
fs.prototype.writeStream = ($) => {
    $ = $ || {};
    const wstream = new istream.Writable();
    wstream.buffer = Buffer.from('');
    wstream.buffers = [];
    wstream._write = (chunk, enc, next) => {
        wstream.buffers.push(Buffer.from(chunk, enc));
        wstream.buffer = Buffer.concat(wstream.buffers);
        const ret = $.onWrite(chunk, enc, next) || ($ => true);
        if (ret) { next(); }
    };
    wstream.onFinish = $.onComplete || ($ => $);
    wstream.on("finish", $$ => { wstream.onFinish(wstream); });
    return (wstream);
};
const duplex = ($) => {
    const dd = new istream.Duplex();
    dd.$read = dd.read;
    dd._read = ($) => { };
    dd.read = (size) => {
        const old = dd._readableState.highWaterMark;
        dd._readableState.highWaterMark = size || old;
        dd.once("data", $ => dd._readableState.highWaterMark = old);
        return (dd.$read(size));
    };
    dd._write = (chunk, enc, next) => {
        dd.push(Buffer.from(chunk, enc));
        next();
    };
    dd.on("finish", $$ => {
        dd.push(null);
        console.log("done writing");
    });
    return (dd);
};
const readBuffer = (buffer) => {
    const rs = new require("stream").Readable();
    rs._read = ($) => { };
    rs.$read = rs.read;
    rs.read = (size) => {
        const old = rs._readableState.highWaterMark;
        rs._readableState.highWaterMark = size || old;
        rs.once("data", $ => rs._readableState.highWaterMark = old);
        return (rs.$read(size));
    };
    rs.push(buffer);
    rs.push(null);
    return (rs);
};
const writeBuffer = ($ = {}) => {
    const buffer = Buffer.from($.data) || Buffer.from("");
    const onWrite = $.onWrite || ($ => $);
    const done = $.done || ($ => $);
    buffer = [buffer];
    const ws = new require("stream").Writable();
    ws._write = (chunk, enc, next) => {
        buffer.push(Buffer.from(chunk, enc));
        onWrite(Buffer.concat(buffer));
        next();
    };
    ws.on("finish", $$ => done(Buffer.concat(buffer)));
    return (ws);
};
fs.prototype.createReadStream = (function () {
    //
});
fs.prototype.createWriteStream = (function (path, options = {}) {
    path = this.resolve(path.toString() || path);
    options = {
        flags: options.flags || 'w',
        encoding: options.encoding || options || 'utf8',
        fd: options.fd || null,
        mode: options.mode || 0o666,
        autoClose: options.autoClose || true,
        start: options.start || 0
    };
    const meta = this.meta(path);
    if (meta === -1) { return (-1); }
    let access = this._checkPermission(path, "write");
    if (access !== true) { return (access); }
    //
    const ws = writeBuffer({
        onWrite: buffer => {
            this.writeFileSync(path, buffer, "buffer");
        },
        done: buffer => {
            //
        }
    });
});