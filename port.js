const port = ({ coport, onConnect, onData, onDisconnect }) => {
    const $ = {};
    $.onConnect = onConnect || ($ => $);
    $.onData = onData || ($ => $);
    $.onDisconnect = onDisconnect || ($ => $);
    $.coport = null;
    $.active = false;
    $.connect = (port) => {
        if ($.active) { return (false); }
        if (port.active) { return (false); }
        $.coport = port;
        $.active = true;
        $.coport.coport = $;
        $.coport.active = true;
        $.onConnect();
        $.coport.onConnect();
        return (true);
    };
    $.disconnect = () => {
        if (!$.active) { return (false); }
        const port = $.coport;
        $.coport = null;
        $.active = false;
        port.coport = null;
        port.active = false;
        port.onDisconnect();
        return (true);
    };
    $.send = (data) => {
        if (!$.active) { return (false); }
        $.coport.onData(clone(data));
    };
    return ($);
};