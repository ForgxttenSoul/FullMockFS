var iproc = require("iproc");

class OS {
    constructor() {
        this.processes = iproc();
    }
    newProcess(name, code) {
        var process = new Process(this.processes, name);
        process.setCode(code);
        return (process);
    }
}
class Process {
    constructor(sys_procs, name) {
        this.sysp = sys_procs;
        var p = this;
        var ops = {
            name: name || "",
            exec: () => {
                postMessage(0);
                postMessage(Object.keys(this));
            },
            onmessge: (msg) => {
                console.log(msg);
            }
        };
        this.id = this.sysp.create(ops);
    }
    setCode(fun) { this._code = fun; }
    start() { this.sysp.start(this.id); }
    stop() { this.sysp.stop(this.id); }
    send(msg) { this.sysp.message(this.id, msg); }
    setOnMessage(fun) { this._onMessage = fun; }
    kill() { this.sysp.destroy(this.id); }
}

tinyworker = require("tiny-worker");
var w = new tinyworker(function () { });
setTimeout(() => { w.terminate(); }, 2000);

class run {
    constructor(op = {}) {
        var code = op.code || ``;
        var restrict = `
            //
            this.global=null;
            this.process=null;
        `;

        code = `${restrict}${code}`;

        new tinyworker(code);
    }
}

new run({
    code: `
        //
        console.log("1");
    `
});