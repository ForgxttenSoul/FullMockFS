Here we go. :)

**This is a COMPLETE mock FileSystem w/some features missing.**

The Mock FileSystem takes advantage of `Buffer` allowing you to have any kind of file from, images, to videos, to songs, to regular txt documents, or exe applications.

(This project is a WIP, if you would like to contribute, you may contact me at: admin@cryogena.net or on Discord by SquishyProxy#2588)

(This project was thought up after I saw a few ftp modules that take optional filesystems allowing the actual ftp servers for virtual fileystems)

**Deprecated methods are excluded unless someone wants to contribute**

```
    50 / 71 Complete (FUNCTIONAL)
    mkdirSync : Done
    chmodSync : Done
    rmdirSync : Done
    chownSync : Done
    existsSync : Done
    statSync : Done
    copyFileSync : Done
    openSync : Done
    lstatSync : Done
    readdirSync : Done
    closeSync : Done
    readSync : Done
    writeSync : Done
    constants : Done
    accessSync : Done
    readFileSync : Done
    linkSync : Done
    unlinkSync : Done
    symlinkSync : Done
    writeFileSync : Done
    appendFileSync : Done
    readlinkSync : Done
    realpathSync : Done
    renameSync : Done
    truncateSync : Done
    utimesSync : Done
    mkdtempSync : Not Done (!)
    // extras (!)
    watch : Not Done
    watchFile : Not Done
    unwatchFile : Not Done
    createReadStream : Not Done (*)
    createWriteStream : Not Done (*)
    // fd methods (i)
    fstatSync : Not Done
    fchmodSync : Not Done
    fchownSync : Not Done
    futimesSync : Not Done
    fsyncSync : Not Done
    ftruncateSync : Not Done
    fdataSync : Not Done
    // async fd methods (i)
    fdata : Not Done
    fsync : Not Done
    ftruncate : Not Done
    futimes : Not Done
    fstat : Not Done
    fchmod : Not Done
    fchown : Not Done
    // async versions
    mkdir : Done
    rmdir : Done
    readdir : Done
    writeFile : Done
    readFile : Done
    stat : Done
    access : Done
    appendFile : Done
    chmod : Done
    chown : Done
    close : Done
    copyFile : Done
    link : Done
    lstat : Done
    mkdtemp : Not Done (!)
    open : Done
    readlink : Done
    realpath : Done
    rename : Done
    symlink : Done
    truncate : Done
    unlink : Done
    utimes : Done
    write : Done
    read : Done
```

(!) Difficulty with these functions
(i) TBD